# -*- coding: utf-8 -*-
"""
Created on Wed Dec 30 12:14:25 2020

@author: Untamed Yeti
"""

#grabs the parent game of a romset from your directory.
import xml.etree.cElementTree as ET
import pandas as pd
import glob
import os
from shutil import copyfile

# Main Path of Game Directory 
path = r'F:\Stage' #source of your files
#file extention of file
fileEXT ='.zip' #.zip,
os.chdir(path)
dst = r'C:\Users\Tc\Desktop\test' # where you want to copy files to

#Full Path of Parent Dat File to be Parsed
#destination
file = r"H:\ROMVault_V2.6.3\DATRoot\MAME_progettosnaps\MAME\MAME 0.219 (Arcade).dat"
tree = ET.parse(file)
root = tree.getroot()

#Parsing out file
for child in root:
    print(child.tag, child.attrib)
    
gl = []
for game in root.iter('machine'): # 'game' for NoIntro 'machine' for MAME
    gl.append(game.attrib)

#print(gl)
    
#Putting results in DataFrame
print ('Printing Data Frame')
x = pd.DataFrame(data=gl, index=None, columns=None, dtype=None, copy=False)
print(x)

#putting games in a list
y = x[x.cloneof.isnull()]
y = y[y.ismechanical.isnull()] #mame only commit out for no-intro
#y = y[y['isdevice'].isnull()] #mame only commit out for no-intro
#y = y[y['isbios'].isnull()]  #mame only commit out for no-intro
y = y[y['sampleof'].isnull()] #mame only commit out for no-intro

#copying games over
for name in y:
    if '(Proto)' not in name and '(Unl)' not in name and '[BIOS]' not in name and '(Beta)' not in name \
    and '(demo)' not in name and '(Preview)' not in name and '(Program)' not in name and '(Unknown)' not in name \
    and '(Manual)' not in name:
        file = name+fileEXT
        for f in glob.glob('*'):
            if name in f:
                try:
                    copyfile(file, os.path.join(dst, file))
                    print('Copying - {}'.format(file))
                except:
                    next
print ('done')




y = y[y['isbios'].notnull()]  #mame only commit out for no-intro
y = y['name']
y
